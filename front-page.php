<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/front-header'); ?>

<main id="main-content">
	<article>
		<section class="experience">
			<h2><?php the_field('value_proposition'); ?></h2>
			<p>Where Paul Has Spoken</p>
			<div class="logos">
				<?php while ( have_rows('logos') ) : the_row(); $logo = get_sub_field('logo'); ?>
					<div class="logo"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" /></div>
				<?php endwhile; ?>
			</div>
		</section>
		<hr>
		<section class="contents">
			<?php the_content(); ?>
		</section>
		<hr>
		<?php $image = get_field('expertise_background'); ?>
		<section class="expertise" style="background-image: url(<?php echo $image['sizes']['xlarge'] ?>);">
			<div>
				<?php the_field('expertise'); ?>
			</div>
		</section>
		<section id="contact-form" class="contact">
			<div>
				<div>
					<h2>Get in touch with Paul</h2>
				</div>
				<div>
					<?php echo do_shortcode('[gravityform id=4 title=false description=false]'); ?>
				</div>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>