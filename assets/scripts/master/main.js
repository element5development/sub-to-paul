var $ = jQuery;

$(document).ready(function () {
	// SELECT FEILD VALIDATION
	$("select").change(function () {
		$(this).addClass('LV_valid_field');
	})

	// CONFERENCE LOGO SLIDER
	$('.logos').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [{
			breakpoint: 500,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		}]
	});

});