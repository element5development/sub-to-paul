<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'xlarge' ); ?>

<header class="front-head post-head <?php if ( has_post_thumbnail() ): ?>has-background-image<?php endif; ?>" style="background-image: url('<?php echo $image[0]; ?>')">
	<div>
		<h1><?php the_title(); ?></h1>
		<hr>
		<p><?php the_field('message'); ?></p>
	</div>
</header>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>