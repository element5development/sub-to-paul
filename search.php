<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php echo 'Search results for: ' . get_search_query(); ?></h1>
</header>

<main id="main-content">
	<article>
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="search-result <?php echo get_post_type(); ?>">
				<header>
					<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
				</header>
				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div>
			</article>
		<?php endwhile; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>