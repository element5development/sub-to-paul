<?php
/*----------------------------------------------------------------*\
		REMOVE ADMIN BAR NODES
\*----------------------------------------------------------------*/
add_filter('autoptimize_filter_toolbar_show','__return_false');

/*----------------------------------------------------------------*\
		REMOVE UNNEEDED DASHBOARD WIDGETS
\*----------------------------------------------------------------*/
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/*----------------------------------------------------------------*\
		DEVELOPER DASHBOARD WIDGET
\*----------------------------------------------------------------*/
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Need Help? Have Questions?', 'custom_dashboard_help');
}
function custom_dashboard_help() {
	echo '<p>When the time comes you need to update your site or have some question on how to use WordPress or just confused feel free to contact us.<br><a href="mailto:support@element5digital.com">support@element5digital.com</a><br><a href="tel:+12485301000">(248) 530-1000</a><br><a target="_blank" href="https://element5digital.com/">element5digital.com</a><img src="/wp-content/themes/starting-point/dist/images/element5-logo.svg" alt="Element5 Digital" />';
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

/*----------------------------------------------------------------*\
		CUSTOM CSS FOR WP ADMIN AREA
\*----------------------------------------------------------------*/
function my_custom_admin() {
  echo '
		<style>
			#wp-optimize-wrap .wpo-page { 
				padding-right: 0; 
			}
			#wp-optimize-wrap .wpo-main-header {
				position: relative;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				width: 100%;
				margin-bottom: 40px;
			}
			.kaui #poststuff #side-sortables .postbox .hndle {
				padding: 15px !important;
				font-size: 13px !important;
				font-family: inherit !important;
				color: #191e23 !important;
			}
			.kaui #poststuff #side-sortables .postbox:not(#submitdiv) .inside {
				padding: 15px !important;
				margin: 0 !important;
			}
			.edit-post-layout {
				padding-top: 46px;
			}
			.edit-post-header {
				position: fixed;
				left: 280px !important;
				right: 0;
				top: 46px !important;
			}
			.edit-post-layout__content {
				left: 280px !important;
				margin-left: 0 !important;
			}
			.edit-post-sidebar {
				top: 102px;
			}
			.components-notice-list {
				left: 280px !important;
			}
			.components-font-size-picker__buttons {
				flex-wrap: wrap;
			}
			.block-editor__container {
				min-height: 100vh !important;
				padding-top: 56px !important;
			}
			.kaui #adminmenuback, 
			.kaui #adminmenu, 
			.kaui #adminmenuwrap {
				background-color: #424242;
			}
			.kaui #adminmenu {
				border-right: 2px solid #dddddd;
			}
			.kaui #adminmenu li.wp-menu-separator div {
				background: #dddddd;
			}
			.kaui #adminmenu div.wp-menu-image::before {
				opacity: .75;
			}
			.kaui #adminmenu li:hover div.wp-menu-image::before {
				opacity: 1;
			}
			.kaui #adminmenu li.menu-top > a:focus, 
			.kaui #adminmenu li a:hover, 
			.kaui #adminmenu li a:hover,
			.kaui #adminmenu > li > a, 
			.kaui #adminmenu ul.wp-submenu>li>a,
			.kaui #adminmenu li a:hover,
			.kaui #adminmenu li.opensub > a.menu-top,
			.kaui #adminmenu li > a.menu-top:focus,
			.kaui #adminmenu .wp-submenu li a:focus, 
			.kaui #adminmenu .wp-submenu li a:hover,
			.kaui #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, 
			.kaui #adminmenu li.wp-has-current-submenu.wp-menu-open a.wp-has-current-submenu,
			.kaui #adminmenu .opensub .wp-submenu li.current a, 
			.kaui #adminmenu .wp-submenu li.current, 
			.kaui #adminmenu .wp-submenu li.current a, 
			.kaui #adminmenu .wp-submenu li.current a:focus, .kaui #adminmenu .wp-submenu li.current a:hover, 
			.kaui #adminmenu a.wp-has-current-submenu:focus + .wp-submenu li.current a {
				color: #fff !important;
			}
			.kaui #adminmenu .wp-submenu li.current a, 
			.kaui #adminmenu .wp-submenu li.current a:focus, .kaui #adminmenu .wp-submenu li.current a:hover,
			.kaui #adminmenu .wp-submenu li a:hover {
				text-decoration: underline !important;
			}
			.kaui #adminmenu>li>a.wp-first-item {
				color: #fff !important;
			}
			.kaui #adminmenu li#toplevel_page_kodeo-home {
				background: #4ebf51 !important;
			}
			.kaui #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, 
			.kaui #adminmenu li.wp-has-current-submenu.wp-menu-open a.wp-has-current-submenu,
			.kaui #adminmenu li.wp-has-submenu.open,
			.kaui #adminmenu li.current a.menu-top {
				background: #1094c5 !important;
			}
			.kaui #adminmenu .wp-submenu, 
			.kaui #adminmenu a.wp-has-current-submenu:focus+.wp-submenu {
				background: #34bbee !important;
				border-top: 1px solid #34bbee !important;
				border-bottom: 1px solid #34bbee !important;
			}
			.kaui #adminmenu #collapse-menu {
				border-top: 1px solid #dddddd !important;
			}
			.kaui #adminmenu #collapse-menu:hover span, 
			.kaui #adminmenu #collapse-menu:hover #collapse-button div::after {
				color: #fff !important;
			}
			.components-notice-list {
				left: 280px;
			}
			.kaui .components-color-palette__item-wrapper .components-color-palette__item,
			.kaui :hover .components-color-palette__item-wrapper .components-color-palette__item  {
				box-shadow: inset 0 0 0 14px !important;
			}
			.kaui #screen-meta-links {
				right: 220px !important;
			}
			.kaui #wpbody-content > div#screen-meta {
				z-index: 100;
			}
			.acf-postbox.seamless {
				background: transparent !important;
				margin-bottom: 50px;
			}
			.kaui #TB_window, .kaui #wp-auth-check, .kaui #wp-link-wrap {
				left: 250px !important;
			}
		</style>
	';
}
add_action('admin_head', 'my_custom_admin');