<?php
/*----------------------------------------------------------------*\
		ENQUEUE JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.0', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), '1.0', true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
		ENABLE HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support( 'html5', array( 
	'comment-list', 
	'comment-form', 
	'search-form', 
	'gallery', 
	'caption' 
) );

/*----------------------------------------------------------------*\
		ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------------*\
		ENABLE RSS FEEDS
\*----------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );

/*----------------------------------------------------------------*\
		ENABLE HTML TITLE TAG
\*----------------------------------------------------------------*/
add_theme_support( 'title-tag' );

/*----------------------------------------------------------------*\
		ENABLE SELECTIVE REFRESH FOR WIDGETS
\*----------------------------------------------------------------*/
add_theme_support( 'customize-selective-refresh-widgets' );

/*----------------------------------------------------------------*\
		ENABLE EDITOR STYLES
\*----------------------------------------------------------------*/
add_theme_support('editor-styles');

/*----------------------------------------------------------------*\
		ENABLE DARK UI STYLES
\*----------------------------------------------------------------*/
//add_theme_support( 'dark-editor-style' );

/*----------------------------------------------------------------*\
		ENQUEUE EDITOR STYLES
\*----------------------------------------------------------------*/
add_editor_style( 'style-editor.css' );

/*----------------------------------------------------------------*\
		REMOVE H2 FROM DEFAULT WORDPRESS PAGINATION
\*----------------------------------------------------------------*/
function clean_pagination() {
	$thePagination = get_the_posts_pagination();
	echo preg_replace('~(<h2\\s(class="screen-reader-text")(.*)[$>])(.*)(</h2>)~ui', '', $thePagination);
 } 