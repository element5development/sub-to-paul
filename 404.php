<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<div>
		<h1>Let's not make this page a recurring thing...</h1>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/404.png ?>" />
		<a href="<?php echo get_home_url(); ?>" class="button">Back To Homepage</a>
	</div>
</header>


<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>