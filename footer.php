<?php 
/*----------------------------------------------------------------*\

	HTML FOOTER CONTENT
	Commonly only used to close off open containers
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<div id="element5-credit">
	<a target="_blank" href="https://element5digital.com">
		<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
	</a>
</div>

<?php wp_footer(); ?>

</body>

</html>