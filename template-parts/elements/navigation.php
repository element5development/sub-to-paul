<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<nav class="primary">
	<a  href="<?php echo home_url(); ?>" class="logo">
		<svg>
			<use xlink:href="#logo" />
		</svg>
	</a>
	<div>
		<p><?php echo get_bloginfo( 'description', 'display' ); ?></p>
		<a href="<?php echo get_home_url(); ?>/#contact-form" class="button">Connect With Paul</a>
	</div>
</nav>