<?php 
/*----------------------------------------------------------------*\

		FRONT PAGE HEADER
		Display the page title

\*----------------------------------------------------------------*/
?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'xlarge' ); ?>

<header class="front-head post-head <?php if ( has_post_thumbnail() ): ?>has-background-image<?php endif; ?>" style="background-image: url('<?php echo $image[0]; ?>')">
	<div>
		<div>
			<h1><?php the_title(); ?></h1>
			<hr>
			<p><?php the_field('sub_header'); ?></p>
			<a href="<?php echo get_home_url(); ?>/#contact-form" class="button">Connect With Paul</a>
		</div>
	</div>
</header>