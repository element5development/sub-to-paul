<?php 
/*----------------------------------------------------------------*\

		POST FOOTER
		Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div class="copyright">
		<p>
			ęCopyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.
			<a href="<?php echo get_privacy_policy_url() ?>">Privacy</a> | 
			<a href="/bio-and-headshots/">Downloads</a>
		</p>
	</div>
	<div class="container">
		<a href="<?php the_permalink(396); ?>" class="button">Book Paul to Speak</a>
		<div class="social">
			<?php if ( get_field('facebook','option') ) : ?>
				<a target="_blank" href="<?php the_field('facebook','option'); ?>" class="social-icon facebook">
					<svg>
						<use xlink:href="#facebook" />
					</svg>
				</a>
			<?php endif; ?>
			<?php if ( get_field('twitter','option') ) : ?>
				<a target="_blank" href="<?php the_field('twitter','option'); ?>" class="social-icon twitter">
					<svg>
						<use xlink:href="#twitter" />
					</svg>
				</a>
			<?php endif; ?>
			<?php if ( get_field('linkedin','option') ) : ?>
			<a target="_blank" href="<?php the_field('linkedin','option'); ?>" class="social-icon linkedin">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
			<?php endif; ?>
			<?php if ( get_field('instagram','option') ) : ?>
			<a target="_blank" href="<?php the_field('instagram','option'); ?>" class="social-icon instagram">
				<svg>
					<use xlink:href="#instagram" />
				</svg>
			</a>
			<?php endif; ?>
			<?php if ( get_field('youtube','option') ) : ?>
			<a target="_blank" href="<?php the_field('youtube','option'); ?>" class="social-icon youtube">
				<svg>
					<use xlink:href="#youtube" />
				</svg>
			</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="dino-easter-egg">
		<svg>
			<use xlink:href="#dino" />
		</svg>
	</div>
</footer>